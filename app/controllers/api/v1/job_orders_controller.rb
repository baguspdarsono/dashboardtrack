class Api::V1::JobOrdersController < ApplicationController

    def index
      @job_order = JobOrder.all
      render json: {
          values: @job_order,
          message: "Sukses"
      }, status: 200
    end

    def show
      @job_order =JobOrder.find_by_id(params[:id])
        if @job_order.present?
          render json:{
            values: @job_order,
            message: "Sukses",
          }, status: 200
        else
          render json:{
            values: "",
            message: "Data Tidak Ditemukan",
          }, status: 400
        end
    end


    def create
      @job_order = JobOrder.new(job_order_params)

      if @job_order.save
        render json:{
          values: "",
          message: "Sukses",
        }, status: 200
      else
        render json:{
          values: "",
          message: "Gagal",
        }, status: 400
      end
    end

    def update
      @job_order = JobOrder.find(params[:id])

      if @job_order.update(job_order_params)
        render json:{
          values: {},
          message: "Sukses",
        }, status: 200
      else
        render json:{
          values: {},
          message: "Gagal",
        }, status: 400
      end
    end

    def destroy
      @job_order = JobOrder.find(params[:id])

      if @job_order.destroy
        render json:{
          values: {},
          message: "Sukses",
        }, status: 200
      else
        render json:{
          values: {},
          message: "Gagal",
        }, status: 400
      end
    end

    def job_order_params
      params.permit(:jobnumber, :staff, :time, :status, :customer, :point)
    end

    # Method untuk handling not found data untuk rescue handling diatas
    def notFound
      render json: {
          values: {},
          message: "Data Not Found!"
      }, status: 400
    end

    rescue_from ActiveRecord::RecordNotFound, with: :notFound
  end
