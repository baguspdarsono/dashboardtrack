json.extract! job_order, :id, :jobnumber, :staff, :customer, :time, :point, :status, :created_at, :updated_at
json.url job_order_url(job_order, format: :json)
