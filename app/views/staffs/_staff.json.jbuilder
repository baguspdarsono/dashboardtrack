json.extract! staff, :id, :name, :address, :active, :imei, :created_at, :updated_at
json.url staff_url(staff, format: :json)
