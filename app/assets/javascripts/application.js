//= require rails-ujs
//= require activestorage
//= require materialize-sprockets
//= require turbolinks
//= require_tree .

//= require flatpickr

$(document).on('turbolinks:load', function() {
    M.Modal._count = 0;
    $('.modal').modal({
        'dismissible': false,
        'preventScrollig': true
    });
    $('.sidenav').sidenav();
});

document.addEventListener('turbolinks:before-render', () => {
    const elem = document.querySelector('#slide-out');
    const instance = M.Sidenav.getInstance(elem);
    if (instance) {
        instance.destroy();
    }
});