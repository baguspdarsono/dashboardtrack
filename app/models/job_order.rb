class JobOrder < ApplicationRecord
  belongs_to :staff
  belongs_to :customer

  enum status: { pending:0, on_progress:1, completed:2}
end
