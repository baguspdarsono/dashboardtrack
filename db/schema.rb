# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_29_130200) do

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.string "point"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "job_orders", force: :cascade do |t|
    t.integer "jobnumber"
    t.string "staff"
    t.datetime "time"
    t.integer "status"
    t.string "customer"
    t.float "point"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "staff_id"
    t.integer "customer_id"
    t.index ["customer_id"], name: "index_job_orders_on_customer_id"
    t.index ["staff_id"], name: "index_job_orders_on_staff_id"
  end

  create_table "staffs", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.string "active"
    t.integer "imei"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
