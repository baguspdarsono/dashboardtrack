class ChangeStatusToBeIntegerInJobOrders < ActiveRecord::Migration[5.2]
  def change
    change_column :job_orders, :status, :integer
  end
end
