class Addtable < ActiveRecord::Migration[5.2]
  def change
    create_table :job_orders do |t|
      t.integer :jobnumber
      t.string :staff
      t.datetime :time
      t.string :status
      t.string :customer
      t.float :point

      t.timestamps
    end
  end
end
