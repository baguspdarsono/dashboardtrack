class AddForeignKeyCustomerToJobOrder < ActiveRecord::Migration[5.2]
  def change
    add_reference :job_orders, :customer, index: true, foreign_key: true
  end
end
