require "application_system_test_case"

class JobOrdersTest < ApplicationSystemTestCase
  setup do
    @job_order = job_orders(:one)
  end

  test "visiting the index" do
    visit job_orders_url
    assert_selector "h1", text: "Job Orders"
  end

  test "creating a Job order" do
    visit job_orders_url
    click_on "New Job Order"

    fill_in "Customer", with: @job_order.customer
    fill_in "Description", with: @job_order.description
    fill_in "Jobnumber", with: @job_order.jobnumber
    fill_in "Staff", with: @job_order.staff
    fill_in "Time", with: @job_order.time
    click_on "Create Job order"

    assert_text "Job order was successfully created"
    click_on "Back"
  end

  test "updating a Job order" do
    visit job_orders_url
    click_on "Edit", match: :first

    fill_in "Customer", with: @job_order.customer
    fill_in "Description", with: @job_order.description
    fill_in "Jobnumber", with: @job_order.jobnumber
    fill_in "Staff", with: @job_order.staff
    fill_in "Time", with: @job_order.time
    click_on "Update Job order"

    assert_text "Job order was successfully updated"
    click_on "Back"
  end

  test "destroying a Job order" do
    visit job_orders_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Job order was successfully destroyed"
  end
end
