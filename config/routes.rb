Rails.application.routes.draw do
  resources :customers
  resources :staffs
  resources :job_orders
  root to: 'job_orders#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      get 'jo' => 'job_orders#index'
      get 'jo/:id' => 'job_orders#show'
      post 'jo' => 'job_orders#create'
      put 'jo/:id' => 'job_orders#update'
      delete 'jo/:id' => 'job_orders#destroy'
    end
  end
end
